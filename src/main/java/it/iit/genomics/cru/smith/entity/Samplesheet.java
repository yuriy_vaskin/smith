package it.iit.genomics.cru.smith.entity;
// Generated Aug 29, 2011 3:51:18 PM by Hibernate Tools 3.2.1.GA

import java.util.Date;

/**
 * 
 */
public class Samplesheet implements java.io.Serializable {

    private int counter;
    private String fcid;
    private int lane;
    private String sampleId;
    private String sampleRef;
    private String indexed;
    private String description;
    private String control;
    private String recipe;
    private String operator;
    private String sampleProject;
    private Date date;
    private String instrument;
    private String experimentType;

    public Samplesheet() {
    }

    public Samplesheet(int counter, String fcid, int lane, String sampleId) {
        this.counter = counter;
        this.fcid = fcid;
        this.lane = lane;
        this.sampleId = sampleId;
    }

    public Samplesheet(int counter, String fcid, int lane, String sampleId, String sampleRef, String indexed, String description, String control, String recipe, String operator, String sampleProject, Date date, String instrument, String experimentType) {
        this.counter = counter;
        this.fcid = fcid;
        this.lane = lane;
        this.sampleId = sampleId;
        this.sampleRef = sampleRef;
        this.indexed = indexed;
        this.description = description;
        this.control = control;
        this.recipe = recipe;
        this.operator = operator;
        this.sampleProject = sampleProject;
        this.date = date;
        this.instrument = instrument;
        this.experimentType = experimentType;
    }

    public int getCounter() {
        return this.counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getFcid() {
        return this.fcid;
    }

    public void setFcid(String fcid) {
        this.fcid = fcid;
    }

    public int getLane() {
        return this.lane;
    }

    public void setLane(int lane) {
        this.lane = lane;
    }

    public String getSampleId() {
        return this.sampleId;
    }

    public void setSampleId(String sampleId) {
        this.sampleId = sampleId;
    }

    public String getSampleRef() {
        return this.sampleRef;
    }

    public void setSampleRef(String sampleRef) {
        this.sampleRef = sampleRef;
    }

    public String getIndexed() {
        return this.indexed;
    }

    public void setIndexed(String indexed) {
        this.indexed = indexed;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getControl() {
        return this.control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    public String getRecipe() {
        return this.recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public String getOperator() {
        return this.operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getSampleProject() {
        return this.sampleProject;
    }

    public void setSampleProject(String sampleProject) {
        this.sampleProject = sampleProject;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getInstrument() {
        return this.instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getCSV() {
        //FCID	Lane	SampleID	SampleRef	Indexed	Description	Control	Recipe	Operator	SampleProject
        StringBuilder sb = new StringBuilder();
        sb.append(fcid + "," + lane + "," + sampleId + "," + sampleRef + "," + indexed + "," + description + "," + control + "," + recipe + "," + operator + "," + sampleProject);
        return sb.toString();
    }

    public static String getCSVHeader() {
        return "FCID,Lane,SampleID,SampleRef,Indexed,Description,Control,Recipe,Operator,SampleProject";

    }

    /**
     * @return the experimentType
     */
    public String getExperimentType() {
        return experimentType;
    }

    /**
     * @param experimentType the experimentType to set
     */
    public void setExperimentType(String experimentType) {
        this.experimentType = experimentType;
    }

}
