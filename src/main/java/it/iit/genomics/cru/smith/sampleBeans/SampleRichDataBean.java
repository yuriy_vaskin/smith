package it.iit.genomics.cru.smith.sampleBeans;

import it.iit.genomics.cru.smith.defaults.NgsLimsUtility;
import it.iit.genomics.cru.smith.defaults.Preferences;
import it.iit.genomics.cru.smith.entity.AttributeValue;
import it.iit.genomics.cru.smith.entity.Collaboration;
import it.iit.genomics.cru.smith.entity.Project;
import it.iit.genomics.cru.smith.entity.Sample;
import it.iit.genomics.cru.smith.hibernate.HibernateUtil;
import it.iit.genomics.cru.smith.userBeans.LoggedUser;
import it.iit.genomics.cru.smith.userBeans.LoggedUserBean;
import it.iit.genomics.cru.smith.userBeans.UserHelper;
import java.io.Serializable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * @(#)SampleRichDataBean.java 20 JUN 2014 Copyright 2014 Computational Research
 * Unit of IIT@SEMM. All rights reserved. Use is subject to MIT license terms.
 *
 * Backing bean for management of attribute value pairs.
 *
 * @author Francesco Venco
 * @version 1.0
 * @since 1.0
 */
@ManagedBean(name = "sampleRichDataBean")
@RequestScoped
public class SampleRichDataBean extends SampleFormBean implements Serializable {

    private DataModel attributevalues;

    /**
    * Bean constructor.
    *
    * @author Francesco Venco
    * @since 1.0
    */
    public SampleRichDataBean() {
        if(Preferences.getVerbose()){
            System.out.println("init SampleRichDataBean");
        }
        
        this.init();

    }

    /**
    * init
    *
    * @author Francesco Venco
    * @since 1.0
    */
    public void init() {
        formId = "SampleAttributesForm";

        FacesContext context = FacesContext.getCurrentInstance();
        String sid = (String) context.getExternalContext().getRequestParameterMap().get("sid");
        if (sid != null) {
            sampleID = Integer.parseInt(sid);
        } // this should never happen and will return an error
        else {
            sampleID = -1;
        }

        //System.out.println("Attribute value bean: load attribute values");
        this.loadAttributeValues();

        //load and check if has loading permission for sample
        //if(!load(true)) return;
        //System.out.println("Attribute value bean: load sample");
        load(true);
        //System.out.println("Attribute value bean : loading done");

        //get the user of the sample
        user = UserHelper.getUserByID(loadedSample.getUser().getId());

        //retrieve all the user's informations
        userLogin = user.getLogin();
        userName = user.getUserName();
        userEmail = user.getMailAddress();
        userTel = user.getPhone();
        pi = user.getPi();
        piLogin = UserHelper.getUserByID(pi).getLogin();

    }

    /**
    * Saves experiment name and time step information for the current sample.
    *
    * @author Francesco Venco
    * @since 1.0
    */
    public void saveExperimentInfo() {

        Session session = HibernateUtil.getSessionFactory().openSession();;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            // load the sample in this session
            Sample s = (Sample) session.load(Sample.class, sampleID);
            //update the sample          
            s.setExperimentName(experiment);
            Date d = null;
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH-MM-SS");
            if (timestep != null) {
                try {
                    d = df.parse(timestep);
                } catch (ParseException ex) {
                    Logger.getLogger(SampleFormBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            s.setTimeSeriesStep(d);
            tx.commit();
            NgsLimsUtility.setSuccessMessage(formId, "ExperimentSaveButton", "Sample update successful", sampleName + " saved correctly");

        } catch (RuntimeException e) {
            tx.rollback();
            e.printStackTrace();
            String fail = e.getMessage();
            NgsLimsUtility.setFailMessage(formId, "ExperimentSaveButton", "Saving Failed" + sampleName, fail);
        } finally {
            session.close();
        }

    }

    /**
    * Loads attribute value pairs for the current sample.
    *
    * @author Francesco Venco
    * @since 1.0
    */
    public void loadAttributeValues() {
        attributevalues = new ListDataModel(AttributevaluesHelper.getAttributvalues(sampleID));

    }

    /**
    * Getter for attributevalues.
    *
    * @author Francesco Venco
    * @return DataModel
    * @since 1.0
    */
    public DataModel getAttributevalues() {
        return this.attributevalues;
    }

    /**
    * Getter for the value of an attribute.
    *
    * @author Francesco Venco
    * @param av
    * @return String
    * @since 1.0
    */
    public String getValue(AttributeValue av) {
        String toRet = "";
        if (av.getValue() != null && !av.getValue().equals("")) {
            toRet = av.getValue();
        } else {
            toRet = "" + av.getNumericValue();
        }
        return toRet;
    }

    /**
    * Return true if the current user has the permission to modify the sample.
    *
    * @author Francesco Venco
    * @return boolean
    * @since 1.0
    */
    public boolean getHasModifyPermission() {
        FacesContext context = FacesContext.getCurrentInstance();        
        LoggedUser lu = ((LoggedUser) context.getApplication().evaluateExpressionGet(context, "#{loggedUserBean}", LoggedUserBean.class)); 
        //LoggedUser lu = new LoggedUser();

        // if the logged user is an admin
        if (lu.getUserRole().equals(Preferences.ROLE_ADMIN)) {
            return true;
        }

        if (lu.getUserRole().equals(Preferences.ROLE_TECHNICIAN)) {
            return false;
        }

        if (loadedSample == null) {
            return false;
        }

        // if the user of the sample and the logged user are the same
        if (user.getId().equals(lu.getUserId())) {
            return true;
        }

        // if the logged user is the PI of the sample's user        
        if (lu.getLogin().equals(piLogin)) {
            return true;
        }

        // finally check for the projects and
        // eventually the permission to modify samples
        ArrayList<Collaboration> allCols = new ArrayList(lu.getCollaborations());
        for (Collaboration c : allCols) {
            if (c.getModifyPermission() > 0) {
                Project p = c.getProject();
                for (Sample s : p.getSamples()) {
                    if (s.getId().equals(sampleID)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

}
