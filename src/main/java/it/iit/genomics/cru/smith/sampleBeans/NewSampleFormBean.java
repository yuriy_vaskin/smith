package it.iit.genomics.cru.smith.sampleBeans;

import it.iit.genomics.cru.smith.defaults.NgsLimsUtility;
import it.iit.genomics.cru.smith.defaults.Preferences;
import it.iit.genomics.cru.smith.entity.Application;
import it.iit.genomics.cru.smith.entity.MultipleRequest;
import it.iit.genomics.cru.smith.entity.MultipleRequestId;
import it.iit.genomics.cru.smith.entity.Sample;
import it.iit.genomics.cru.smith.entity.SequencingIndex;
import it.iit.genomics.cru.smith.hibernate.HibernateUtil;
import it.iit.genomics.cru.smith.userBeans.LoggedUser;
import it.iit.genomics.cru.smith.userBeans.LoggedUserBean;
import it.iit.genomics.cru.smith.userBeans.UserHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * @(#)NewSampleFormBean.java 20 JUN 2014 Copyright 2014 Computational Research
 * Unit of IIT@SEMM. All rights reserved. Use is subject to MIT license terms.
 *
 * Backing bean for new sample form.
 *
 * @author Francesco Venco
 * @version 1.0
 * @since 1.0
 */
@ManagedBean(name = "newSampleFormBean")
//@RequestScoped
//@SessionScoped
@ViewScoped
public class NewSampleFormBean extends SampleFormBean implements Serializable {

    private int tab1Index;
    private int tab2Index;
    SampleSearchBean sampleSearchBean;

    /**
    * Bean constructor.
    *
    * @author Francesco Venco
    * @since 1.0
    */
    public NewSampleFormBean() {
        if(Preferences.getVerbose()){
            System.out.println("init NewSampleFormBean");
        }
        init();
    }

    /**
    * init.
    *
    * @author Francesco Venco
    * @since 1.0
    */
    @Override
    public void init() {
        //System.out.println("init NewSampleFormBean");
        bundle = ResourceBundle.getBundle("it.iit.genomics.cru.smith.Messages.Messages");
        formId = "RequestForm";

        // retrieve informations on the logged user
        FacesContext context = FacesContext.getCurrentInstance();
        sampleSearchBean = (SampleSearchBean) context.getApplication().evaluateExpressionGet(context, "#{sampleSearchBean}", SampleSearchBean.class);    
        LoggedUser loggedUserBean = ((LoggedUser) context.getApplication().evaluateExpressionGet(context, "#{loggedUserBean}", LoggedUserBean.class)); 
        
        //LoggedUser loggedUserBean = new LoggedUser();

        userLogin = loggedUserBean.getLogin();
        userName = loggedUserBean.getUsername();
        userEmail = loggedUserBean.getMailadress();
        userTel = loggedUserBean.getPhone();
        pi = loggedUserBean.getPi();
        piLogin = loggedUserBean.getLoggedUserPIName();
        user = loggedUserBean.getLoggedUser();

        status = "queued";

        selectedIndexes = new ArrayList<SampleSpecDesc>();
        selectedIndexes.add(new SampleSpecDesc("none", "none", "none"));

        //load all the applications and indexes
        loadApplications();
        loadIndexes();      
        costCenter = UserHelper.getPi(UserHelper.getUserByLoginName(userLogin)).getUserName();
        
        biodate = Calendar.getInstance().getTime();

    }

    //persists a new request entry TODO to be moved?
    /**
    * Persists a new sample to the database.
    *
    * @author Francesco Venco
    * @return String - a redirect to the samplesCreated page
    * @since 1.0
    */
    public String submit() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        String fail = "";
        String newSamplesIds = "";
        try {
            tx = session.beginTransaction();
            //Sample s = new Sample();
            ArrayList<Sample> newSamples = new ArrayList<Sample>();
            for (int i = 0; i < selectedIndexes.size(); i++) {
                newSamples.add(new Sample());
            }

            //validate the form
            if (!validateMultipleForms()) {
                RuntimeException e = new RuntimeException("Errors in the form");
                throw e;
            }

            //application management
            Application a = getCurrentApplication();
            if (isNewApplication(a)) {
                session.save(a);
            } else {
                //load the existing application
                a = (Application) session.load(Application.class, findApplicationID(a));
                session.saveOrUpdate(a);
            }
            //update sample(s) for indexes and application
            for (int i = 0; i < selectedIndexes.size(); i++) {
                Sample s = newSamples.get(i);
                SampleSpecDesc ssd = selectedIndexes.get(i);
                System.out.println("USING " + ssd);
                s.setApplication(a);
                setSampleData(s, ssd);
                if (selectedIndexes.size() > 1) {
                    s.setName(s.getName() + "_" + ssd.getIndex());
                }
                SequencingIndex si = (SequencingIndex) session.load(SequencingIndex.class, findIndexID(ssd.getIndex()));
                s.setSequencingIndexes(si); 
                s.setId(SampleHelper.getNextSampleId());
                session.save(s);
                newSamplesIds = newSamplesIds + " " + s.getId();
                s = addSampleIdToSampleName(s);
                session.saveOrUpdate(s);
            }

            //if there is more than one selected index, a multiple request is needed to be
            //inserted in the database
            if (selectedIndexes.size() > 1) {
                int newRequestId = 1;
                List<MultipleRequest> totalRequests = session.createQuery("from MultipleRequest").list();
                //find the last used ID
                /*for(Multiplerequest mr : totalRequests){
                 if(mr.getId().getRequestId() >= newRequestId)
                 newRequestId = mr.getId().getRequestId() + 1;
                 }*/
                if (!totalRequests.isEmpty()) {
                    MultipleRequest last = totalRequests.get(totalRequests.size() - 1);
                    newRequestId = last.getId().getRequestId() + 1;
                }
                //make the new multiple requests 
                for (int i = 0; i < selectedIndexes.size(); i++) {
                    System.out.println("adding request " + newRequestId);
                    Sample s = newSamples.get(i);
                    int sampleid = s.getId();
                    MultipleRequest mr = new MultipleRequest();
                    MultipleRequestId mrId = new MultipleRequestId();
                    mrId.setRequestId(newRequestId);
                    mrId.setSampleId(sampleid);
                    mr.setId(mrId);
                    mr.setSample(s);
                    session.save(mr);
                }

            }

            tx.commit();
            NgsLimsUtility.setSuccessMessage(formId, "NGSRequestssubmitbutton", "Sample request successful",
                    sampleName + " inserted correctly with id(s) " + newSamplesIds);
            NgsLimsUtility.setSuccessMessage("sampleTableForm", "NGSRequestssubmitbutton", "Sample request successful",
                    sampleName + " inserted correctly with id(s) " + newSamplesIds);

        } catch (RuntimeException e) {
            tx.rollback();
            e.printStackTrace();
            fail = fail + " " + e.getMessage();
            NgsLimsUtility.setFailMessage(formId, "NGSRequestssubmitbutton", fail, sampleName + " not inserted");
            loadApplications();
            return null;
        } finally {
            session.close();
        }

        loadApplications();
        sampleSearchBean.initSampleList();

        //return "samplesSearch?faces-redirect=true";
        //return "sampleDetails";
        //return "samplesCreated?faces-redirect=true?sids=" + newSamplesIds;
        return "samplesCreated?sids=" + newSamplesIds + " faces-redirect=true";

    }

    

    /**
    * Getter for tab1Index.
    *
    * @author Francesco Venco
    * @return int
    * @since 1.0
    */
    public int getTab1Index() {
        return tab1Index;
    }

    /**
    * Setter for tab1Index.
    *
    * @author Francesco Venco
    * @param tab1Index
    * @since 1.0
    */
    public void setTab1Index(int tab1Index) {
        this.tab1Index = tab1Index;
    }

    /**
    * Getter for tab2Index.
    *
    * @author Francesco Venco
    * @return int
    * @since 1.0
    */
    public int getTab2Index() {
        return tab2Index;
    }

    /**
    * Setter for tab2Index.
    *
    * @author Francesco Venco
    * @param tab2Index
    * @since 1.0
    */
    public void setTab2Index(int tab2Index) {
        this.tab2Index = tab2Index;
    }
}
