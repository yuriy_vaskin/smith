package it.iit.genomics.cru.smith;

import it.iit.genomics.cru.smith.defaults.NgsLimsUtility;
import it.iit.genomics.cru.smith.defaults.Preferences;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Francesco
 */
@ManagedBean
@SessionScoped
public class TestBean {
    
    private int i;
    
    public TestBean(){
        if(Preferences.getVerbose()){
            System.out.println("init TestBean");
        }
      
    }
    
    public void load(){
        i++;
        
    }
    
    public int getI(){
        return this.i;
    }
    
    public void setI(int i){
        this.i = i;
    }
    
    public void save(){
         NgsLimsUtility.setFailMessage("thisForm", "test", "test", "test");         
     }
    
    
}
