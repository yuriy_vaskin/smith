package it.iit.genomics.cru.smith.utils;

import it.iit.genomics.cru.analysis.AnalysisManager;
import it.iit.genomics.cru.analysis.DailyTask;
import it.iit.genomics.cru.analysis.UpdateManager;
//import static it.iit.genomics.cru.smith.utils.TimerServlet.MINS_10;

import java.sql.Date;
import java.util.Timer;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
/**
 *
 * @author yvaskin
 */
@ManagedBean(name = "contextListener")
@ApplicationScoped
public class ContextListener implements ServletContextListener {
    
    private Timer timer = null;
    public static final long HOURS_6 = 6 * 3600 * 1000;
    public static final long HOURS_1 = 1 * 3600 * 1000;
    public static final long MINS_1 = 60 * 1000;
    public static final long MINS_10 = 600 * 1000;
    public static final long SECS_10 = 10 * 1000;
    
    public void contextInitialized(ServletContextEvent event) {
     if (timer == null) {
            timer = new Timer();
            DailyTask dt = new DailyTask();
            FacesContext context = FacesContext.getCurrentInstance();
            AnalysisManager analysisManager = (AnalysisManager) context.getApplication().evaluateExpressionGet(context, "#{analysisManager}", AnalysisManager.class);
            //if(analysisManager == null){
            //    analysisManager = new AnalysisManager();
            //    System.out.println("new AnalysisManager created");
            //}
            dt.setAnalysisManager(analysisManager);
            //timer.scheduleAtFixedRate(dt, new Date(System.currentTimeMillis()), HOURS_6);
            //timer.scheduleAtFixedRate(dt, new Date(System.currentTimeMillis()), HOURS_1);
            //timer.scheduleAtFixedRate(dt,   new Date(System.currentTimeMillis()),   SECS_10); 
            //timer.scheduleAtFixedRate(dt,   new Date(System.currentTimeMillis()),   MINS_1); 
            timer.scheduleAtFixedRate(dt,   new Date(System.currentTimeMillis()),   MINS_10); 
            
        }
    }
 
    public void contextDestroyed(ServletContextEvent event) {
        timer.cancel();
        timer = null;
    }    
}
