package it.iit.genomics.cru.smith.utils;

import it.iit.genomics.cru.smith.entity.Sample;
import java.io.Serializable;
import org.hibernate.HibernateException;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.id.IdentityGenerator;


/**
 *
 * @author yvaskin
 */
public class UseIdOrGenerate extends IdentityGenerator {

@Override
public Serializable generate(SessionImplementor session, Object obj) throws HibernateException {
    if (obj == null) throw new HibernateException(new NullPointerException()) ;

    if ((((Sample) obj).getId()) == 0) {
        Serializable id = super.generate(session, obj) ;
        return id;
    } else {
        return ((Sample) obj).getId();

    }
}
}