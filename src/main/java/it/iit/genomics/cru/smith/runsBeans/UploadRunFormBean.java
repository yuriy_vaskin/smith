package it.iit.genomics.cru.smith.runsBeans;

import it.iit.genomics.cru.smith.defaults.NgsLimsUtility;
import it.iit.genomics.cru.smith.defaults.Preferences;
import it.iit.genomics.cru.smith.entity.Lane;
import it.iit.genomics.cru.smith.entity.Reagent;
import it.iit.genomics.cru.smith.entity.Sample;
import it.iit.genomics.cru.smith.entity.SampleRun;
import it.iit.genomics.cru.smith.entity.SampleRunId;
import it.iit.genomics.cru.smith.entity.SequencingIndex;
import it.iit.genomics.cru.smith.entity.User;
import it.iit.genomics.cru.smith.hibernate.HibernateUtil;
import it.iit.genomics.cru.smith.reagentsBeans.ReagentHelper;
import it.iit.genomics.cru.smith.sampleBeans.SampleHelper;
import it.iit.genomics.cru.smith.userBeans.UserHelper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 * @(#)UploadRunFormBean.java 20 JUN 2014 Copyright 2014 Computational Research
 * Unit of IIT@SEMM. All rights reserved. Use is subject to MIT license terms.
 *
 * Backing bean for uploading sample runs.
 *
 * @author Heiko Muller
 * @version 1.0
 * @since 1.0
 */
@ManagedBean(name = "uploadRunFormBean")
@SessionScoped
public class UploadRunFormBean implements Serializable {

    private ResourceBundle bundle;
    private String destination;
    private String applicationPath;
    private UploadedFile file1;
    private String filename = "undefined";
    private String filecontent;
    private String userLogin = "";
    private User operator;
    private String formId = "";
    private int runID = -1;
    RunsSearchBean rsb;

    /**
    * Bean constructor
    *
    * @author Heiko Muller
    * @since 1.0
    */
    public UploadRunFormBean() {
        if(Preferences.getVerbose()){
            System.out.println("init UploadRunFormBean");
        }
        
        FacesContext context = FacesContext.getCurrentInstance();
        //loggedUserBean = new LoggedUser();
        rsb = (RunsSearchBean) context.getApplication().evaluateExpressionGet(context, "#{runsSearchBean}", RunsSearchBean.class);  
        userLogin = context.getExternalContext().getRemoteUser();
        operator = UserHelper.getUserByLoginName(userLogin);
        formId = "runTableUploadForm";
        //List<SampleRun> runs = RunHelper.getRunsList();
        //if (runs != null && runs.size() > 0) {
        runID = rsb.getNewRunId();
        bundle = ResourceBundle.getBundle("it.iit.genomics.cru.smith.Messages.Messages");
        //} else {
        //    runID = -1;
        //}
        try {
            applicationPath = context.getExternalContext().getRealPath("/");
            destination = applicationPath + "upload" + File.separator;
            if(Preferences.getVerbose()){
                System.out.println(destination);
            }            
        } catch (UnsupportedOperationException uoe) {
            uoe.printStackTrace();
        }
    }

    /**
    * Action listener for file upload event.
    *
    * @author Heiko Muller
    * @param event - a file upload event
    * @since 1.0
    */
    public void handleFileUpload(FileUploadEvent event) {
        System.out.println("uploading");
        file1 = event.getFile();
        if (file1 != null) {
            FacesMessage msg = new FacesMessage("Successful", file1.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        System.out.println("getting data");
        transferFile(file1);
        //annotationname1 = file1.getFileName();
        filename = (new File(file1.getFileName())).getName();
        System.out.println(filename);
        System.out.println("upload completed");

    }

    /**
    * Copies remote file to upload directory.
    *
    * @author Heiko Muller
    * @param file - a remote file
    * @since 1.0
    */
    public void transferFile(UploadedFile file) {
        //String fileName = file.getFileName();
        String fileName = (new File(file.getFileName())).getName();
        filename = fileName;
        try {
            InputStream in = file.getInputstream();
            OutputStream out = new FileOutputStream(new File(destination + fileName));

            int reader = 0;
            byte[] bytes = new byte[(int) file.getSize()];
            while ((reader = in.read(bytes)) != -1) {
                out.write(bytes, 0, reader);
            }
            in.close();
            out.flush();
            out.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
    * Parses a comma separated sample sheet.
    *
    * @author Heiko Muller
    * @return String[][]
    * @since 1.0
    */
    private String[][] parseRequestCSV() {
        File f = new File(destination + filename);
        return readFile(readFile(f));
    }

    /**
    * Reads a file.
    *
    * @author Heiko Muller
    * @param f
    * @return Vector<String> - the file content with each line of text in a Vector<String>
    * @since 1.0
    */
    private Vector<String> readFile(File f) {
        Vector<String> v = new Vector<String>();
        try {
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            br.readLine();
            String line = "";
            while ((line = br.readLine()) != null) {
                v.add(line);
            }
        } catch (IOException ioe) {
        }
        return v;
    }

    /**
    * Reads file content and returns it as comma separated values.
    *
    * @author Heiko Muller
    * @param v - file content
    * @return String[][] - the file content with each line of text in a Vector<String>
    * @since 1.0
    */
    private String[][] readFile(Vector<String> v) {
        String[][] result = new String[v.size()][21];
        for (int i = 0; i < v.size(); i++) {
            result[i] = v.get(i).split(",");
        }
        return result;
    }

    /**
    * Submits a sample run.
    *
    * @author Heiko Muller
    * @since 1.0
    */
    public void submitSampleRun() {
        //0 Lane	
        //1 ID	
        //2 UserName	
        //3 Flowcell	
        //4 Cluster gen	
        //5 Sequencing	
        //6 Run Date	
        //7 Barcode	
        //8 SampleName
        formId = "runsTableForm";
        ArrayList<SampleRun> toBeSubmitted = new ArrayList<SampleRun>();
        boolean allSampleRunsCorrect = true;
        StringBuilder error = new StringBuilder();

        if (operator != null && (operator.getUserRole().equals(Preferences.ROLE_TECHNICIAN) || operator.getUserRole().equals(Preferences.ROLE_ADMIN))) {
            String[][] data = parseRequestCSV();
            for (int i = 0; i < data.length; i++) {
                if (!data[i][0].startsWith("Lane")) {
                    int sampleid = Integer.parseInt(data[i][1]);
                    Sample sample = SampleHelper.getSampleByID(sampleid);
                    User user = sample.getUser();
                    if (user != null) {
                        SampleRun sr = setSampleRun(data[i], error);
                        if(sr != null){
                            toBeSubmitted.add(sr);
                        }else{
                            System.out.println("error in sample " + sr.getsamId());
                            allSampleRunsCorrect = false;
                        }

                    } else {
                        NgsLimsUtility.setFailMessage(formId, "RunUploadProcessButton", bundle.getString("user.not.found1"), bundle.getString("user.not.found2"));
                    }
                }
            }

        }
        
        if(allSampleRunsCorrect){
            for(SampleRun sr : toBeSubmitted){
                submitSampleRun(sr);
            }
        }else{
            NgsLimsUtility.setFailMessage(formId, "RunUploadProcessButton", "Error uploading a run", error.toString());
        }
        rsb.init();
    }

    /**
    * Sets current sample run data.
    *
    * @author Heiko Muller
    * @param s - a row of the uploaded request csv file
    * @return SampleRun
    * @since 1.0
    */
    private SampleRun setSampleRun(String[] s, StringBuilder error) {
        //0 Lane	
        //1 ID	
        //2 UserName	
        //3 Flowcell	
        //4 Cluster gen	
        //5 Sequencing	
        //6 Run Date	
        //7 Barcode	
        //8 SampleName
        SampleRun sr = new SampleRun();
        try {  
            Integer id = Integer.parseInt(s[1]);
            Sample samp = SampleHelper.getSampleByID(id);
            if(samp == null){
                //sample not found in database
                error.append(id + ": " + "sample not found in database");
                return null;
            }
            samp = testIndex(samp, s[7]);
            if(samp == null){
                //this happens when a conflict between database record and uploaded index is found;
                error.append(id + ": " + "a conflict between database record and uploaded index is found");
                return null;
            }
            
            Lane l = new Lane();
            l.setLaneName(s[0]);
            HashSet<Lane> hsl = new HashSet<Lane>();
            hsl.add(l);
            sr.setLanes(hsl);

            
            sr.setUser(operator);
            sr.setsample(samp);
            SampleRunId sid = new SampleRunId();
            sid.setRunId(runID);
            sid.setSamId(samp.getId());
            sr.setId(sid);

            sr.setFlowcell(s[3]);
            sr.setControl(false);

            Reagent cg = testReagent(s[4], "Cluster generation");
            sr.setReagentByClustergenerationReagentCode(cg);
            //Reagent sp = testReagent(s[4], "Sequencing");
            //sr.setReagentBySamplePrepReagentCode(sp);
            Reagent sq = testReagent(s[5], "Sequencing");
            sr.setReagentBySequencingReagentCode(sq);
            String runfoldername = filename.replace(".csv", "");
            sr.setRunFolder(runfoldername);          
            
        }catch(IndexOutOfBoundsException e){
            e.printStackTrace();
            error.append("Unhandled error");
            return null;
        }

        return sr;
    }
    
    /**
    * Tests if reagent is found in database. If not, it is added with limited information.
    *
    * @author Heiko Muller
    * @param reagent
    * @param application - the uploaded sequencing index
    * @return Reagent
    * @since 1.0
    */
    private Reagent testReagent(String reagent, String application){
        Reagent result = null;
        
        if(reagent == null || reagent.length() == 0){
            //uploaded field for reagent is empty
            result = new Reagent();
            result.setApplication(application);
            result.setReagentBarCode("unknown");
            result.setCatalogueNumber("unknown");
            result.setComments("Reagent added automatically by LIMS. Please add missing information.");
            result.setCostCenter("unknown");
            result.setReceptionDate(new Date(System.currentTimeMillis()));
            result.setExpirationDate(new Date(System.currentTimeMillis()));
            result.setInstitute("unknown");
            result.setPrice(0);
            result.setSupportedReactions(1000);
            result.setUserByOperatorUserId(operator);
            result.setUserByOwnerId(operator);
            ReagentHelper.saveOrUpdateReagent(result);
        }
        result = ReagentHelper.getReagentsByBarcode(reagent);
        if(result != null){
            //reagent was found
            return result;
        }else{
            //indicated reagent is missing from database, add it
            result = new Reagent();
            result.setApplication(application);
            result.setReagentBarCode(reagent);
            result.setCatalogueNumber("unknown");
            result.setComments("Reagent added automatically by LIMS. Please add missing information.");
            result.setCostCenter("unknown");
            result.setReceptionDate(new Date(System.currentTimeMillis()));
            result.setExpirationDate(new Date(System.currentTimeMillis()));
            result.setInstitute("unknown");
            result.setPrice(0);
            result.setSupportedReactions(1000);
            result.setUserByOperatorUserId(operator);
            result.setUserByOwnerId(operator);
            ReagentHelper.saveOrUpdateReagent(result);
            
        }
        return result;
        
    
    }
    
    /**
    * Tests correctness of sequencing index information.
    *
    * @author Heiko Muller
    * @param sample
    * @param index - the uploaded sequencing index
    * @return Sample
    * @since 1.0
    */
    private Sample testIndex(Sample sample, String index){
        if(index == null){
            index = new String("none");
        }
        if(index != null && index.length() == 0){
            index = new String("none");
        }
        
        //first, remove any whitespace from the uploaded index
        index = index.trim();
        if(index.length() > 6){
            //the index field in the uploaded form does not contain a sequencing index
            NgsLimsUtility.setFailMessage(formId, "RunUploadProcessButton", "index error", "index too long " + index);
            return null;
        }
        
        //second, make sure uploaded index seqquences are in upper case letters, except for "none"
        if(!index.equals("none")){
            index = index.toUpperCase();
        }
        
        //Retrieve the index sequence of the sample
        SequencingIndex idx = sample.getSequencingIndexes();
        
        //cases to handle:
        
        //case 1:
        //uploaded index and sample index may be the same.
        //In that case just return.
        if(idx != null && idx.getIndex().equals(index)){
            return sample;
        }else if(idx == null && index.equals("none")){
            //this should never happen because all samples have at least the null index "none"
            SequencingIndex idex = SampleHelper.getSequencingIndexIdBySequence(index); 
            if(idex == null){
                //the index sequence wasn't found in the database.
                NgsLimsUtility.setFailMessage(formId, "RunUploadProcessButton", "index error", "unknown index " + index);
                return null;
            }
            sample.setSequencingIndexes(idex);
            SampleHelper.updateSample(sample);
            return sample;
        }
           
        //case 2: 
        //idx may be null/none because the facility added the index to the sample without updating the database record.
        //In that case update the database record for sample using the index sequence in the uploaded run form.
        if(idx != null && idx.getIndex().equals("none") && index.length() == 6){
            //System.out.println("updating sample index using uploaded sample run form information. Using " + index);
            SequencingIndex idex = SampleHelper.getSequencingIndexIdBySequence(index);  
            if(idex == null){
                //the index sequence wasn't found in the database.
                NgsLimsUtility.setFailMessage(formId, "RunUploadProcessButton", "index error", "unknown index " + index);
                return null;
            }
            sample.setSequencingIndexes(idex);
            SampleHelper.updateSample(sample);
            return sample;
        }else if(idx == null && index.length() == 6){
            //this should never happen because all samples have at least the null index "none"
            SequencingIndex idex = SampleHelper.getSequencingIndexIdBySequence(index);   
            if(idex == null){
                //the index sequence wasn't found in the database.
                NgsLimsUtility.setFailMessage(formId, "RunUploadProcessButton", "index error", "unknown index " + index);
                return null;
            }
            sample.setSequencingIndexes(idex);
            SampleHelper.updateSample(sample);
            return sample;
        }
        
        //case 3: 
        //idx may differ from the uploaded index. In that case return null and alert the staff to resolve the conflict.
        //Either they need to update the database record for the sample because a user reported a mistake or the staff made a mistake in the run form.        
        else if(idx != null && index.length() == 6 && !idx.getIndex().equals(index)){
            NgsLimsUtility.setFailMessage(formId, "RunUploadProcessButton", "index conflict", "conflict for index " + index);
            return null;
        }
        NgsLimsUtility.setFailMessage(formId, "RunUploadProcessButton", "unknown index error", "error for index " + index);
        return null;//this handles unforeseen cases. Alert the staff about the problem.
    }

    /**
    * Submits sample run.
    *
    * @author Heiko Muller
    * @param sr - a sample run
    * @return String
    * @since 1.0
    */
    public String submit(SampleRun sr) {

        Session session = HibernateUtil.getSessionFactory().openSession();

        Transaction tx = null;
        String fail = "";
        try {
            tx = session.beginTransaction();
            boolean firstSample = true;

            // set the id object
            SampleRunId sid = new SampleRunId();
            sid.setRunId(runID);
            sid.setSamId(sr.getsamId());




            // set the sample to queued and (re)set sequencing index
            Sample sample = (Sample) session.load(Sample.class, sr.getsamId());
            sample.setStatus("running");
            session.update(sample);
            session.save(sr);
            
            //lane management
            Set<Lane> ls = sr.getLanes();
            Iterator it = ls.iterator();
            while(it.hasNext()){
                Lane l = (Lane)it.next();
                l.setSamplerun(sr);
                session.save(l);
            }

            // if good, try to save and commit
            session.update(sr);


            //}
            tx.commit();

            NgsLimsUtility.setSuccessMessage(formId, "submission",
                    "Sample run added", " inserted correctly");
            return "runDetails?rid=" + this.runID + " faces-redirect=true";

        } catch (RuntimeException e) {
            tx.rollback();
            e.printStackTrace();
            fail = fail + e.getMessage();
            NgsLimsUtility.setFailMessage(formId, "submission", fail,
                    "Run insertion failed ");
            return null;
        } finally {
            session.close();
        }

    }
    
    /**
    * Submits sample run.
    *
    * @author Heiko Muller
    * @param sr - a sample run
    * @return String
    * @since 1.0
    */
    public void submitSampleRun(SampleRun sr) {

        Session session = HibernateUtil.getSessionFactory().openSession();

        Transaction tx = null;
        String fail = "";
        try {
            tx = session.beginTransaction();
            boolean firstSample = true;

            // set the id object
            SampleRunId sid = new SampleRunId();
            sid.setRunId(runID);
            sid.setSamId(sr.getsamId());




            // set the sample to queued and (re)set sequencing index
            Sample sample = (Sample) session.load(Sample.class, sr.getsamId());
            sample.setStatus("running");
            session.update(sample);
            session.save(sr);
            
            //lane management
            Set<Lane> ls = sr.getLanes();
            Iterator it = ls.iterator();
            while(it.hasNext()){
                Lane l = (Lane)it.next();
                l.setSamplerun(sr);
                session.save(l);
            }

            // if good, try to save and commit
            session.update(sr);


            //}
            tx.commit();

            NgsLimsUtility.setSuccessMessage(formId, "submission",
                    "Sample run added", " inserted correctly");
            

        } catch (RuntimeException e) {
            tx.rollback();
            e.printStackTrace();
            fail = fail + e.getMessage();
            NgsLimsUtility.setFailMessage(formId, "submission", fail,
                    "Run insertion failed ");
            
        } finally {
            session.close();
        }

    }

    /**
    * Getter for destination.
    *
    * @author Heiko Muller
    * @return String
    * @since 1.0
    */
    public String getDestination() {
        return destination;
    }

    /**
    * Setter for destination.
    *
    * @author Heiko Muller
    * @param destination
    * @since 1.0
    */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
    * Getter for destination.
    *
    * @author Heiko Muller
    * @return String
    * @since 1.0
    */
    public String getApplicationPath() {
        return applicationPath;
    }

    /**
    * Setter for applicationPath.
    *
    * @author Heiko Muller
    * @param applicationPath
    * @since 1.0
    */
    public void setApplicationPath(String applicationPath) {
        this.applicationPath = applicationPath;
    }

    /**
    * Getter for file1.
    *
    * @author Heiko Muller
    * @return UploadedFile
    * @since 1.0
    */
    public UploadedFile getFile1() {
        return file1;
    }

    /**
    * Setter for file1.
    *
    * @author Heiko Muller
    * @param file1
    * @since 1.0
    */
    public void setFile1(UploadedFile file1) {
        this.file1 = file1;
    }

    /**
    * Getter for filename.
    *
    * @author Heiko Muller
    * @return String
    * @since 1.0
    */
    public String getFilename() {
        return filename;
    }

    /**
    * Setter for filename.
    *
    * @author Heiko Muller
    * @param filename
    * @since 1.0
    */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
    * Getter for filecontent.
    *
    * @author Heiko Muller
    * @return String
    * @since 1.0
    */
    public String getFilecontent() {
        return filecontent;
    }

    /**
    * Setter for filecontent.
    *
    * @author Heiko Muller
    * @param filecontent
    * @since 1.0
    */
    public void setFilecontent(String filecontent) {
        this.filecontent = filecontent;
    }
}
