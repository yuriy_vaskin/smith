package it.iit.genomics.cru.smith.navigationBeans;

import it.iit.genomics.cru.smith.defaults.Preferences;
import it.iit.genomics.cru.smith.userBeans.LoggedUser;
import it.iit.genomics.cru.smith.userBeans.LoggedUserBean;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @(#)NavigationBean.java 20 JUN 2014 Copyright 2014 Computational Research
 * Unit of IIT@SEMM. All rights reserved. Use is subject to MIT license terms.
 *
 * Backing bean for logged user information.
 *
 * @author Francesco Venco
 * @version 1.0
 * @since 1.0
 */
@ManagedBean(name = "navigationBean")
@RequestScoped
public class NavigationBean implements Serializable {
    
    FacesContext context;
    ExternalContext ext;
    LoggedUser loggedUser;

    /**
     * Bean constructor
     *
     * @author Francesco Venco
     * @since 1.0
     */
    public NavigationBean(){
        if(Preferences.getVerbose()){
            System.out.println("init NavigationBean");
        }        
        context = FacesContext.getCurrentInstance();  
        ext = context.getExternalContext();
        loggedUser = (LoggedUser) context.getApplication().evaluateExpressionGet(context, "#{loggedUserBean}", LoggedUserBean.class); 
    }
    
    /**
     * Invalidates HttpSession for logout.
     *
     * @author Francesco Venco
     * @since 1.0
     */
    public void doLogout() {
        HttpSession sess = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        sess.invalidate();
        System.out.println("Logged out");
    }
    
    /**
     * Tests if user is logged in.
     *
     * @author Francesco Venco
     * @return String
     * @since 1.0
     */
    public String getIsLoggedIn(){
        //FacesContext context = FacesContext.getCurrentInstance();       
        //ExternalContext ext = context.getExternalContext();
        //String s = ext.getRemoteUser();
        //System.out.println(s);
        HttpServletRequest request = (HttpServletRequest) ext.getRequest();
        Principal principal = request.getUserPrincipal();
        if(principal == null){
            return "false";
        }
        return "true";
    }
    
    /**
     * Return the login name of the logged user.
     *
     * @author Francesco Venco
     * @return String
     * @since 1.0
     */
    public String getLoginName() {
        //FacesContext context = FacesContext.getCurrentInstance();       
        //ExternalContext ext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) ext.getRequest();
        Principal principal = request.getUserPrincipal();
        if(principal == null){
            return Preferences.ROLE_GUEST;
        }
        
        //LoggedUser lu = (LoggedUser) context.getApplication().evaluateExpressionGet(context, "#{loggedUserBean}", LoggedUserBean.class); 
        //if(loggedUser == null){
            //loggedUser = new LoggedUser();
        loggedUser = (LoggedUser) context.getApplication().evaluateExpressionGet(context, "#{loggedUserBean}", LoggedUserBean.class); 
        //}
        //LoggedUser lu = new LoggedUser();
        return loggedUser.getLogin();
        //return name;
    }
    
    /**
     * Tests if user has add permission.
     *
     * @author Francesco Venco
     * @return boolean
     * @since 1.0
     */
    public boolean getUserHasAddPermission() {
        //System.out.println("User had add permission?");
        if(loggedUser == null){
            loggedUser = new LoggedUser();
        }
        //LoggedUser loggeduser = new LoggedUser();
        return (loggedUser.getIsTech() || loggedUser.getIsAdmin());
    }
    
}